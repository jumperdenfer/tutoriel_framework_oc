<?php
namespace App\Frontend\Modules\News;

use MyFram\BackController;
use MyFram\Http\HttpRequest;
use App\Entity\Comments;
Use App\Form\CommentFormBuilder;
Use MyFram\Form\FormHandler;

class NewsController extends BackController{

    public function executeIndex(HttpRequest $request){
        $nombreNews = $this->app->config->get('nombre_news');
        $nombreCaracteres = $this->app->config->get('nombre_caracteres');

        $this->page->addVar('title','Test de page d\'index');

        $manager =  $this->managers->getManagerOf('News');

        $listeNews = $manager->getList(0, 5);

        foreach ($listeNews as $news)
        {
            if (strlen($news->contenu()) > $nombreCaracteres)
            {
                $debut = substr($news->contenu(), 0, $nombreCaracteres);
                $debut = substr($debut, 0, strrpos($debut, ' ')) . '...';

                $news->setContenu($debut);
            }
        }
        // On ajoute la variable $listeNews à la vue.
        $this->page->addVar('listeNews', $listeNews);
        $this->page->setLayout('Layout');
    }

    public function executeShow(HttpRequest $request){

        $news = $this->managers->getManagerOf('News')->getUnique($request->getData('id'));
        if(empty($news)){
            $this->app->httpResponse()->redirect404();
        }

        $this->page->addVar('title', $news->titre());
        $this->page->addVar('news',$news);
        $this->page->addVar('comments',$this->managers->getManagerOf('Comments')->getListof($news->id()));
        $this->page->setLayout('Layout');
    }


    public function executeInsertComment(HttpRequest $request){
        $this->page->addVar('title','Ajout d\'un commentaire');

        if($request->method() == 'POST'){
            $comment = new Comments([
                'news' => $request->getData('news'),
                'auteur' => $request->postData('auteur'),
                'contenu' => $request->postData('contenu')
            ]);
        }else{
            $comment = new Comments;
        }

        $formBuilder = new CommentFormBuilder($comment);
        $formBuilder->build();

        $form = $formBuilder->form();

        $formHandler = new FormHandler($form,$this->managers->getManagerOf('Comments'),$request);
        if ($formHandler->process())
        {
            $this->app->user->setFlash('Le commentaire a bien été ajouté, merci !');
            $this->app->httpResponse()->redirect('news-'.$request->getData('news').'.html');
        }

        $this->page->addVar('comment', $comment);
        $this->page->addVar('form', $form->createView());
        $this->page->addVar('title', 'Ajout d\'un commentaire');
        $this->page->setLayout('Layout');
     }
}
