<?php
namespace App\Frontend\Middleware;
use MyFram\Http\HttpResponse;
use App\Frontend\FrontendApplication;

class TestMiddleware{
    public static function run($app){
        if(!$app->user->isAuthenticated()){
            $app->user->setFlash('test');
        }
    }
}
