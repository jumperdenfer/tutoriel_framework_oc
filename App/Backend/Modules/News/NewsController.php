<?php

namespace App\Backend\Modules\News;

use MyFram\backController;
use MyFram\Http\HttpRequest;
use MyFram\Form\FormHandler;

use App\Entity\Comments;
use App\Entity\News;

use App\Form\CommentFormBuilder;
use App\Form\NewFormBuilder;


class NewsController extends BackController{
    public function executeIndex(HttpRequest $request){

        $this->page->addVar('title','Gestion des news');
        $manager = $this->managers->getManagerOf('News');

        $this->page->addVar('listeNews',$manager->getList());
        $this->page->addVar('nombreNews',$manager->count());
        $this->page->setLayout('Layout');
    }

    public function executeInsert(HttpRequest $request){

        $this->processForm($request);

        $this->page->addVar("title","Ajout d'une news");
        $this->page->setLayout('Layout');
    }

    public function executeUpdate(HttpRequest $request){
        $this->processForm($request);
        $this->page->addVar('title','Modification d\'une news');
        $this->page->setLayout('Layout');
    }

    public function processForm(HttpRequest $request){
        if($request->method() == 'POST'){
            $news = new News([
                'auteur' => $request->postData('auteur'),
                'titre' => $request->postData('titre'),
                'contenu' => $request->postData('contenu')
            ]);
            if($request->getExists('id')){
                $news->setId($request->getData('id'));
            }
        }else{
            if($request->getExists('id')){
                $news = $this->managers->getManagerOf('News')->getUnique($request->getData('id'));
            }
            else{
                $news = new News();
            }
        }

        $formBuilder = new NewFormBuilder($news);
        $formBuilder->build();

        $form = $formBuilder->form();
        $formHandler = new FormHandler($form,$this->managers->getManagerOf('News'),$request);
        if($formHandler->process()){
            $this->app->user->setFlash($news->isNew() ? 'La news à bien été ajoutée !' : 'La news à bien été modifiée !');
            $this->app->httpResponse()->redirect('/projet_test/admin/');
        }
        $this->page->addVar('form',$form->createView());
        $this->page->setLayout('Layout');
    }

    public function executeDelete(HttpRequest $request){
        $this->managers->getManagerOf('News')->delete($request->getData('id'));
        $this->app->user->setFlash('La news a bien été supprimée.');
        $this->app->HttpResponse()->redirect('.');
    }

    public function executeUpdateComments(HttpRequest $request){
        $this->page-addVar('title',"Modification d'un commentaire");
        if($request->method == 'POST'){
            $comment = new Comments([
                'id' => $request->getData('id'),
                'auteur' => $request->postData('auteur'),
                'contenu' => $request->postData('contenu')
            ]);
        }
        else{
            $comment = $this->managers->getManagerOf('Comments')->get($request->getData('id'));
        }

        $formBuilder = new CommentFormBuilder($comment);
        $formBuilder->build();

        $form = $formBuilder->form();
        $formHandler = new FormHandler($form,$this->managers->getManagerOf('Comments'),$request);
        if($formHandler->process()){
            $this->app->user->setFlash('Le commentaire à bien été modifié');
            $this->app->httpResponse()->redirect('/admin/');
        }
        $this->page->addVar('form', $form->createView());
        $this->page->setLayout('Layout');

    }
    public function executeDeleteComment(HTTPRequest $request)
    {
        $this->managers->getManagerOf('Comments')->delete($request->getData('id'));
        $this->app->user->setFlash('Le commentaire a bien été supprimé !');
        $this->app->httpResponse()->redirect('.');
    }
}
