<?php
namespace App\Backend\Modules\Connexion;

use MyFram\BackController;
use MyFram\Http\HttpRequest;


class ConnexionController extends BackController{
    public function executeIndex(HttpRequest $request){
        $this->page->addVar('title','Connexion');
        if($request->postExists('login')){
            $login = $request->postData('login');
            $password = $request->postData('password');
            if($login == $this->app->config->get('login') && $password == $this->app->config->get('password') ){
                $this->app->user->setAuthenticated(true);
                $this->app->httpResponse()->redirect('.');
            }else{
                $this->app->user->setFlash('Le pseudo ou le mot de passe est incorrect.');
            }
        }
    }
}
