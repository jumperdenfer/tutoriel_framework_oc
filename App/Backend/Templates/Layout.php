<!DOCTYPE html>
<html lang="fr" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>
            <?php echo $title ?? 'Mon super site!'; ?>
        </title>
    </head>
    <body>
        <header>
            <h1><a href='/projet_test/web/'>Mon super site</a></h1>
            <p>il est vide...</p>
        </header>
        <main>
            <nav>
                <ul>
                  <li><a href="/projet_test/web/">Accueil</a></li>
                  <?php if ($user->isAuthenticated()) { ?>
                    <li><a href=".">Admin</a></li>
                    <li><a href="news-insert.html">Ajouter une news</a></li>
                  <?php } ?>
                </ul>
            </nav>

            <div id="content-wrap">
                <section id="main">
                    <?php if ($user->hasFlash()) echo '<p style="text-align: center;">', $user->getFlash(), '</p>'; ?>

                    <?= $content ?>
                </section>
            </div>
        </main>
        <footer>

        </footer>
    </body>
</html>
