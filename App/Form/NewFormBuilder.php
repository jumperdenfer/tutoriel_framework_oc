<?php
namespace App\Form;
use MyFram\Form\FormBuilder;
//Ajout des type de fields
use MyFram\Form\Fields\StringField;
use MyFram\Form\Fields\TextField;
//Ajout des validateur
use MyFram\Form\Validators\NotNullValidator;
use MyFram\Form\Validators\MaxLengthValidator;
class NewFormBuilder extends FormBuilder{
    public function build(){
        $this->form->add(new StringField([
            'label' => 'Auteur',
            'name' => 'auteur',
            'maxLength' => 50,
            'validators' =>[
                new NotNullValidator('Veuillez indiquer l\'auteur'),
                new MaxLengthValidator("Le nom de l'auteur est trop long (50 Charactère maximum)",50)
            ]
        ]))
        ->add(new StringField([
            'label' => 'Titre',
            'name' => 'titre',
            'maxLength' => 50,
            'validators' => [
                new NotNullValidator('Veuillez indiquer le titre'),
                new MaxLengthValidator("Le titre est trop long (50 charactère maximum)",50)
            ]
        ]))
        ->add(new TextField([
            'label' => 'Contenu',
            'name' => 'contenu',
            'cols' => 50,
            'rows' => 7,
            'validators' => [
                new NotNullValidator("Veuillez remplir le champs contenue")
            ]
        ]));
    }
}
