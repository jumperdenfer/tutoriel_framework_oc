<?php
namespace App\Form;
use MyFram\Form\FormBuilder;
//Ajout des fields
use MyFram\Form\Fields\StringField;
use MyFram\Form\Fields\TextField;
//Ajout des validateurs
use MyFram\Form\Validators\NotNullValidator;
use MyFram\Form\Validators\MaxLengthValidator;

class CommentFormBuilder extends FormBuilder{
    public function build(){
        $this->form->add(new StringField([
            'label' => 'Auteur',
            'name' => 'auteur',
            'maxLength' => 50,
            'validators' => [
                new MaxLengthValidator("L'auteur spécifié est trop long (50 charactère maximum)",50),
                new NotNullValidator("Merci de spécifier le nom de l'auteur")
            ]
        ]))
        ->add(new TextField([
            'label' => 'Contenu',
            'name' => 'contenu',
            'rows' => 7,
            'cols' => 50,
            'validators' => [
                new NotNullValidator("Merci de spécifier votre commentaire")
            ]
        ]));
    }

}
