<?php
namespace App\Model\News;

use MyFram\Manager;
use App\Entity\News;

abstract class NewsManager extends Manager{
    abstract public function getList($dabut = -1, $limite =-1);
    abstract public function getUnique($id);
    abstract public function count();
    abstract protected function modify(News $news);
    abstract protected function add(News $news);
    abstract protected function delete($id);

    public function save(News $news){
        if($news->isValid()){
            $news->isNew() ? $this->add($news) : $this->modify($news);
        }
        else{
            throw new \RuntimeException('La news doit êtree validée pour être enregistrée');
        }
    }
}
