<?php
namespace App\Model\News;

use App\Model\News\NewsManager;
use App\Entity\News;
/**
* Afin de rendre plus lisible les requête SQL il est envisageable d'intrégrer un ORM
* par exemple lightOrm pourrait simplifier la lecture (peusdo orm maison)
*/

class NewsManagerPDO extends NewsManager{

    public function getList($debut = -1, $limite = -1)
    {
        $sql = 'SELECT id, auteur, titre, contenu, dateAjout, dateModif FROM news ORDER BY id DESC';

        if ($debut != -1 || $limite != -1)
        {
            $sql .= ' LIMIT '.(int) $limite.' OFFSET '.(int) $debut;
        }

        $requete = $this->dao->query($sql);
        $requete->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, 'App\Entity\News');

        $listeNews = $requete->fetchAll();
        foreach ($listeNews as $news)
        {
            $news->setDateAjout(new \DateTime($news->dateAjout()));
            $news->setDateModif(new \DateTime($news->dateModif()));
        }

        $requete->closeCursor();

        return $listeNews;
    }

    public function getUnique($id)
    {
        $sql = 'SELECT id, auteur,titre,contenu,dateAjout,dateModif FROM news WHERE id = :id';
        $requete = $this->dao->prepare($sql);
        $requete->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, 'App\Entity\News');
        $requete->bindValue(':id', (int) $id, \PDO::PARAM_INT);
        $requete->execute();
        if($news = $requete->fetch()){
            $news->setDateAjout(new \DateTime($news->dateAjout()));
            $news->setDateModif(new \DateTime($news->dateModif()));

            return $news;
        }
        $requete->closeCursor();

        return null;
    }

    public function count()
    {
        return $this->dao->query('SELECT COUNT(*) FROM news')->fetchColumn();
    }

    protected function add(News $news){
        $requete = $this->dao->prepare('INSERT INTO news SET auteur = :auteur, titre = :titre, contenu = :contenu, dateAjout = NOW(),dateModif = NOW()');
        $requete->bindValue(':auteur',$news->auteur());
        $requete->bindValue(':titre',$news->titre());
        $requete->bindValue(':contenu',$news->contenu());
        $requete->execute();
    }

    protected function modify(News $news){
        $requete = $this->dao->prepare('UPDATE news SET auteur = :auteur, titre = :titre, contenu = :contenu, dateModif = NOW() WHERE id = :id');
        $requete->bindValue(':auteur',$news->auteur());
        $requete->bindValue(':titre',$news->titre());
        $requete->bindValue(':contenu',$news->contenu());
        $requete->bindValue(':id',$news->id(),\PDO::PARAM_INT);
        $requete->execute();
    }

    public function delete($id){
        $requete = $this->dao->prepare("DELETE FROM news WHERE id = :id");
        $requete->bindValue(':id',$id);
        $requete->execute();
    }
}
