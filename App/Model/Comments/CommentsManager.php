<?php

namespace App\Model\Comments;
use MyFram\Manager;
use App\Entity\Comments;


abstract class CommentsManager extends Manager{
    abstract protected function add(Comments $comment);
    abstract function getListOf($news);
    abstract protected function modify(Comments $comment);
    abstract public function get($id);
    abstract protected function delete($id);

    public function save(Comments $comment){
        if($comment->isValid()){
            $comment->isNew() ? $this->add($comment) : $this->modify($comment);
        }else{
            throw new \RuntimeException('Le commentaire doit être valide pour être enregistré');
        }
    }
}
