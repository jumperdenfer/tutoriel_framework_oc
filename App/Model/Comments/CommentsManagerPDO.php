<?php
namespace App\Model\Comments;
use App\Model\Comments\CommentsManager;
use App\Entity\Comments;

class CommentsManagerPDO extends CommentsManager{

    protected function add(Comments $comment){
        $query = $this->dao->prepare('INSERT INTO comments SET news = :news,auteur =:auteur, contenu = :contenu, date = NOW()');
        $query->bindValue(':news',$comment->news(), \PDO::PARAM_INT);
        $query->bindValue(':auteur',$comment->auteur());
        $query->bindValue(':contenu',$comment->contenu());
        $query->execute();

        $comment->setId($this->dao->lastInsertId());
    }

    public function getListof($news){
        if(!ctype_digit($news)){
            throw new \InvalidArgumentException('L\'identifiant de la news passé doit être un nombre entier valide');
        }
        $query = $this->dao->prepare('SELECT id, news,auteur,contenu, `date` FROM comments WHERE news = :news');
        $query->bindValue(':news',$news,\PDO::PARAM_INT);
        $query->execute();

        $query->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, 'App\Entity\Comments');

        $comments = $query->fetchAll();
        foreach ($comments as $comment) {
            $comment->setDate(new \DateTime($comment->date()));

        }
        return $comments;
    }

    protected function modify(Comments $comment){
        $requete = $this->dao->prepare("UPDATE comments SET auteur = :auteur, contenu = :contenu WHERE id = :id");
        $requete->bindValue(':auteur',$comment->auteur());
        $requete->bindValue(':contenu',$comment->contenu());
        $requete->bindValue(':id',$comment->id(), \PDO::PARAM_INT);
        $requete->execute();
    }

    public function get($id){
        $requete = $this->dao->prepare('SELECT id, news, auteur, contenue FROM comments WHERE id = :id');
        $requete->bindValue(':id',$id,\PDO::PARAM_INT);
        $requete->execute();

        $requete->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE,'App\Entity\Comment');

        return $requete->fetch();
    }

    public function delete($id){
        $requete = $this->dao->prepare('DELETE FROM comments WHERE id = :id');
        $requete->bindValue(':id',$id,\PDO::PARAM_INT);
        $requete->execute();

    }
}
