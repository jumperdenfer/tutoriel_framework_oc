<?php
namespace MyFram;

class Config extends ApplicationComponent{
    protected $vars = [];
    protected $globalVars = [];
    public function get($var){
        if(!$this->vars){
            //Fichier de configuration Globals,
            $jsonGlobal = \file_get_contents(__DIR__.'/../../App/config/global_app.json');
            $configGlobalElements = json_decode($jsonGlobal);
            //Fichier de configuration par App
            $json = \file_get_contents(__DIR__.'/../../App/'.$this->app->name().'/config/app.json');
            $configElements = json_decode($json);
            foreach ($configElements as $configElement) {
                $this->vars[$configElement->title] = $configElement->value;
            }
            foreach($configGlobalElements as $configGlobalElement){
                $this->globalVars[$configGlobalElement->title] = $configGlobalElement->value;
            }

        }
        //Vérifie si la variable existe dans le fichier de config spécifique à l'app.
        if(isset($this->vars[$var])){
            return $this->vars[$var];
        }
        //Si une variable n'est pas trouvé dans le fichier de config spécifié, alors on vérifie le fichier de config global_app.
        elseif(isset($this->globalVars[$var])){
            return $this->globalVars[$var];
        }
        //Si la variable n'est pas trouvé, retourne null
        return null;
    }
}
