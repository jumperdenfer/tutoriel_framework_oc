<?php
namespace MyFram;
use MyFram\ApplicationComponent;
use MyFram\Managers;

//Génère un code d'erreur du au fait, qu'a cette emplacement, l'autoloader n'est pas chargé
abstract class BackController extends ApplicationComponent{

    protected $action = '';
    protected $module = '';
    protected $page = null;
    protected $view = '';
    protected $managers;

    public function __construct(Application $app,$module,$action){
        parent::__construct($app);

        $this->managers  = new Managers('PDO',PDOFactory::getMysqlConnexion($app),$app->name());

        $this->page = new Page($app);
        $this->setModule($module);
        $this->setAction($action);
        $this->setView($action);

    }

    public function execute(){
        $method = 'execute'.ucFirst($this->action);
        if(!is_callable([$this,$method])){
            throw new \RuntimeException('L\'Action "'.$this->action.'" n\'est pas définie sur ce module');
        }
        $this->$method($this->app->httpRequest());
    }

    public function page(){
        return $this->page;
    }

    public function setModule($module){
        if(!is_string($module) || empty($module)){
            throw new \InvalidArgumentException('Le module doit être une chaine de charactères valide.');
        }
        $this->module = $module;
    }

    public function setAction($action){
        if(!is_string($action) || empty($action)){
            throw new \InvalidArgumentException('L\'action doit être une chaine de charactères valide.');
        }
        $this->action = $action;
    }

    public function setView($view){
        if(!is_string($view) || empty($view)){
            throw new \InvalidArgumentException('la vue doit être une chaine de charactères valide.');
        }
        $this->view = $view;
        $this->page->setContentFile(__DIR__.'./../../App/'.$this->app->name().'/Modules/'.$this->module.'/Views/'.$this->view.'.php');

    }

}
