<?php
namespace MyFram;

use MyFram\ApplicationComponent;
class Page extends ApplicationComponent{
    protected $contentFiles;
    protected $vars = [];
    protected $layout = "Layout";

    public function addVar($var,$value){
        if(!is_string($var) || is_numeric($var) || empty($var)){
            throw new \InvalidArgumentException('Le nom de la variable doit être une chaine de charactères non nulle');
        }
        $this->vars[$var] = $value;
    }

    public function setLayout($layout){
        $this->layout = $layout;
    }

    public function getGeneratedPage(){
        if(!file_exists($this->contentFiles)){
            throw new \RuntimeException('La vue spécifié n\'existe pas');
        }
        $user = $this->app->user;

        extract($this->vars);
        ob_start();
            require $this->contentFiles;
        $content =  ob_get_clean();

        ob_start();
            require __DIR__.'/../../App/'.$this->app->name().'/Templates/'.$this->layout.'.php';
        return ob_get_clean();
    }

    public function setContentFile($contentFiles){
        if(!is_string($contentFiles) || empty($contentFiles)){
            throw new \InvalidArgumentException('La vue spécifié est invalide');
        }
        $this->contentFiles = $contentFiles;
    }
}
