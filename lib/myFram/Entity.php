<?php
namespace MyFram;

class Entity implements \ArrayAccess{
    protected $erreurs = [];
    protected $id;
    use Traits\Hydrator;

    public function __construct(array $donnees = []){
        if(!empty($donnees)){
            $this->hydrate($donnees);
        }
    }


    public function isNew(){
        return empty($this->id);
    }

    public function erreurs(){
        return $this->erreurs;
    }

    public function id(){
        return $this->id;
    }

    public function setId($id){
        $this->id = $id;
    }

    public function offsetGet($var){
        if(isset($this->$var) && is_callable([$this,$var])){
            return $this->$var();
        }
    }

    public function offsetSet($var,$value){
        $method = 'set'.ucFirst($var);
        if(isset($this->$var) && is_callable([$this,$method])){
            $this->$method($value);
        }
    }

    public function offsetExists($var){
        return isset($this->$var) && is_callable([$this, $var]);
    }

    public function offsetUnset($var){
        throw new \Exception('Impossible de supprimer une quelconque valeur');
    }
}
