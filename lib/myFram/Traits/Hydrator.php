<?php
namespace MyFram\Traits;

    trait Hydrator{
    public function hydrate(array $data){
        foreach($data as $key => $value){
            $method = 'set'.ucFirst($key);
            if(is_callable([$this,$method])){
                $this->$method($value);
            }
        }
    }
}
