<?php
namespace MyFram;

class Managers{
    protected $api = null;
    protected $dao = null;
    protected $appName;
    protected $managers = [];

    public function __construct($api,$dao,$appName){
        $this->api = $api;
        $this->dao = $dao;
        $this->appName = $appName;
    }

    public function getManagerOf($module){
        if(!is_string($module) || empty($module)){
            throw new InvalidArgumentException('Le module spécifié est invalide');
        }
        if(!isset($this->managers[$module])){
            //Si un manager est spécifique à un module ( utile dans certain cas)
            if(class_exists('App\\'.$this->appName.'\\Model\\'.$module.'Manager'.$this->api)){
                $manager = 'App\\'.$this->appName.'\\Model\\'.$module.'Manager'.$this->api;
            }
            //Sinon regarde dans les managers global
            else{
                $manager = 'App\\Model\\'.$module.'\\'.$module.'Manager'.$this->api;
            }
            $this->managers[$module] = new $manager($this->dao);
        }
        return $this->managers[$module];
    }
}
