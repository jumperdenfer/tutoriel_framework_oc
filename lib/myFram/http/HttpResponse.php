<?php
namespace MyFram\Http;

use MyFram\ApplicationComponent;
use MyFram\Page;

class HttpResponse extends ApplicationComponent{
    /**
    * addHeader
    * Définie un paramètre Header
    * @param string Header
    * @return void
    */
    public function addHeader($header){
        header($header);
    }
    /**
    * redirect
    * Définie une redirection ( hors 404)
    * @param string Location
    * @return void
    */
    public function redirect($location){
        header('Location: '.$location);
        exit();
    }
    /**
    * redirect4040
    * Définie la redirection 404
    * @param string Location
    * @return void
    */
    public function redirect404(){
        $this->page = new Page($this->app);
        $this->page->setContentFile(__DIR__.'/../../../Error/404.html');
        $this->addHeader('HTTP/1.0 404 Not Found');
        $this->send();
    }

    public function send(){
        exit($this->page->getGeneratedPage());
    }
    /**
    * setCookie
    * Créé un cookie
    * @return void
    */
    public function setCookie($name,$value ='',$expire = 0,$path = null,$domain = null,$secure=false,$httpOnly = true){
        setCookie($name,$value,$expire,$path,$domain,$secure,$httpOnly);
    }
    /**
    * setPage
    * Définie la page
    */
    public function setPage(Page $page){
        $this->page = $page;
    }
}
