<?php
namespace MyFram\Http;

use MyFram\ApplicationComponent;

class HttpRequest extends ApplicationComponent{
    /**
    * cookieData
    *
    * Vérifie l'existence d'un cookie et le return
    * @param key  Nom du cookie
    * @return string||null
    */
    public function cookieData($key){
        return $_COOKIE[$key] ?? null;
    }
    /**
    * cookieExists
    * Vérifie l'existence d'un cookie et renvoie un boolean
    * @param Key Nom du cookie
    * @return Boolean
    */
    public function cookieExists($key){
        return isset($_COOKIE[$key]);
    }

    public function getData($key)
    {
        return $_GET[$key] ?? null;
    }

    public function getExists($key)
    {
        return isset($_GET[$key]);
    }

    /**
    * method
    * Renvoie la method utiliser pour la requête
    * @return string||null;
    */
    public function method(){
        return $_SERVER['REQUEST_METHOD'];
    }
    /**
    * postData
    * Vérifie l'existence d'une données envoyé en post et la renvoie
    * @param Key Nom de la donnée
    * @return string||null
    */
    public function postData($key){
        return $_POST[$key] ?? null;
    }
    /**
    * postExists
    * Vérifie l'existence d'une données envoyé en post
    * @param Key Nom de la donnée
    * @return boolean
    */
    public function postExists($key){
        return isset($_POST[$key]);
    }
    /**
    * requestURI
    * Renvoie l'uri utiliser dans la requête du serveur
    * @return string||null
    */
    public function requestURI(){
        return $_SERVER['REQUEST_URI'];
    }
}
