<?php
namespace MyFram;
class PDOFactory{
    public static function getMysqlConnexion($app){
        $db_host = $app->config->get('db_host');
        $db_name = $app->config->get('db_name');
        $db_user = $app->config->get('db_user');
        $db_pass = $app->config->get('db_pass');
        $db = new \PDO("mysql:host={$db_host};dbname={$db_name}",$db_user, $db_pass);
        $db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        return $db;
    }
}
