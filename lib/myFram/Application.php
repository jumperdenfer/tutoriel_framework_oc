<?php
namespace MyFram;

use MyFram\Http\HttpRequest;
use MyFram\Http\HttpResponse;
use MyFram\Router\Router;
use MyFram\Router\Route;
use MyFram\User;
use MyFram\Config;

abstract class Application{
    protected $httpRequest;
    protected $httpResponse;
    protected $name;
    public $config;
    public $user;

    public function __construct(){
        $this->httpRequest = new HttpRequest($this);
        $this->httpResponse = new HttpResponse($this);
        $this->name = '';
        $this->user = new User();
        $this->config = new Config($this);
    }

    public function getController(){
        $router = new Router;

        $json = file_get_contents(__DIR__.'./../../App/'.$this->name.'/config/route.json');
        $routes = json_decode($json);
        foreach ($routes as $route) {
            $vars = [];
            $middleware = [];
            if($route->vars ?? null){
                $vars = explode(',',$route->vars);
            }
            if($route->middleware ?? null){
                $middleware = explode(',',$route->middleware);
            }
            $router->addRoute(new Route($route->url,$route->module,$route->action,$vars,$middleware));
        }
        try{

            $matchedRoute = $router->getRoute($this->httpRequest->requestURI());

        }catch(\RuntimeException $e){
            if ($e->getCode() == Router::NO_ROUTE){
                $this->httpResponse->redirect404();
                exit();
            }
        }

        $_GET = array_merge($_GET,$matchedRoute->vars());
        //Temporairement écrit ici, reflexion sur l'emplacement exacte à déterminé
        if($matchedRoute->hasMiddleware()){
            $middlewareList = $matchedRoute->middleware();
            $middleware = [];
            foreach($middlewareList as $key){
                $middlewareClass = "App\\".$this->name.'\\Middleware\\'.$key.'Middleware';
                $middlewareClass::run($this);
            }
        }
        $controllerClass = "App\\".$this->name.'\\Modules\\'.$matchedRoute->module().'\\'.$matchedRoute->module().'Controller';
        return new $controllerClass($this,$matchedRoute->module(),$matchedRoute->action());

    }


    abstract public function run();

    public function httpRequest(){
        return $this->httpRequest;
    }
    public function httpResponse(){
        return $this->httpResponse;
    }
    public function name(){
        return $this->name;
    }
}
