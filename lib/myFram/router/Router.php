<?php
namespace MyFram\Router;

use MyFram\Router\Route;

class Router{
    protected $route =[];
    const NO_ROUTE = 1;

    public function addRoute(Route $route){
        if(!in_array($route,$this->route)){
            $this->route[] = $route;
        }
    }

    public function getRoute($url){
        foreach($this->route as $route){

            if( ($varsValue = $route->match($url)) !== false ){
                if($route->hasVars()){
                    $varsName = $route->varsNames();
                    $listVars =[];
                    foreach ($varsValue as $key => $match) {

                        if($key !== 0){
                            $listVars[$varsName[$key - 1]] = $match;
                        }
                    }
                    $route->setVars($listVars);
                }
                return $route;
            }
        }

        throw new \RuntimeException('Aucune route ne correspond à l\'URL', self::NO_ROUTE);
    }
}
