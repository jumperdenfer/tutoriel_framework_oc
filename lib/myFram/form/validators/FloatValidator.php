<?php
namespace MyFram\Form\Validators;
use MyFram\Form\Validator;

class FloatValidator extends Validator{
    public function __construct($errorMessage){
        parent::__construct($errorMessage);
    }

    public function isValid($value){
        return is_float($value);
    }
}
