<?php
namespace MyFram\Form\Validators;
use MyFram\Form\Validator;

class TelValidator extends Validator{

    public function __construct($errorMessage){
        parent::__construct($errorMessage);
    }

    public function isValid($value){
        $phoneFiltered = filter_var($value,FILTER_SANITIZE_NULBER_INT);
        $phoneFiltered = str_replace('-', "", $phoneFiltered);
        if(strlen($phoneFiltered) < 10 || strlen($phoneFiltered) > 14){
            return true;
        }
        else{
            return false;
        }
    }
}
