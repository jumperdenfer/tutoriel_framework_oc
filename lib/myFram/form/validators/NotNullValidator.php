<?php
namespace  MyFram\Form\Validators;
use MyFram\Form\Validator;

class NotNullValidator extends Validator{
    public function isValid($value){
        return $value != '';
    }
}
