<?php
namespace MyFram\Form\Validators;
use MyFram\Form\Validator;

class IntValidator extends Validator{
    public function __construct($errorMessage){
        parent::__construct($errorMessage);
    }

    public function isValid($value){
        return is_int($value);
    }
}
