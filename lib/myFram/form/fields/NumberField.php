<?php
namespace MyFram\Form\Fields;
use MyFram\Form\Field;

class NumberField extends Field{
    protected $min;
    protected $max;
    protected $step;
    public function buildWidget(){
        $widget = '';
        if(!empty($this->errorMessage)){
                $widget .= $this->errorMessage.'<br>';
        }

        $widget .= "<label>{$this->label}</label><input type='number' name='{$this->name}'";
        if(!empty($this->value)){
            $widget .= " value='{$this->value}'";
        }
        if(!empty($this->min)){
            $widget .= " min='{$this->min}'";
        }
        if(!empty($this->max)){
            $widget .= " max='{$this->max}'";
        }
        if(!empty($this->step)){
            $widget .= " step={$this->step}";
        }

        return "{$this->widget} >";
    }
}
