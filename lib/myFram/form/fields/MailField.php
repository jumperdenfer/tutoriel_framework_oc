<?php
namespace MyFram\Form\Field;
class MailField extends Field{
    public function buildWidget(){
        $widget = '';
        if(!empty($this->errorMessage)){
                $widget .= $this->errorMessage.'<br>';
        }

        $widget .= "<label>{$this->label}</label><input type='mail' name='{$this->name}'";
        if(!empty($this->value)){
            $widget .= " value='{$this->value}'";
        }

        return "{$this->widget} >";
    }
}
