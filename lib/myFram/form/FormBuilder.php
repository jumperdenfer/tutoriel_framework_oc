<?php
/**
* Class abstraite FormBuilder, permettant la création des formulaire de façon simplifier
*/
namespace  MyFram\Form;

use MyFram\Entity;
use MyFram\Form\Form;

abstract class FormBuilder{
    protected $form;
    public function __construct(Entity $entity){
        $this->setForm(new Form($entity));
    }

    abstract public function build();

    public function setForm(Form $form){
        $this->form = $form;
    }
    public function form(){
        return $this->form;
    }
}
