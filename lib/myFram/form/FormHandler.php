<?php
namespace MyFram\Form;
use MyFram\Form\Form;
use MyFram\Manager;
use MyFram\Http\HttpRequest;

class FormHandler{
    protected $form;
    protected $manager;
    protected $request;

    public function __construct(Form $form,Manager $manager, HttpRequest $request){
        $this->setForm($form);
        $this->setManager($manager);
        $this->setRequest($request);
    }
    public function process(){
        if($this->request->method() == "POST" && $this->form->isValid()){
            $this->manager->save($this->form->entity());
            return true;
        }
        return false;
    }
    //Setters
    public function setForm(Form $form){
        $this->form = $form;
    }
    public function setManager(Manager $manager){
        $this->manager = $manager;
    }
    public function setRequest(HttpRequest $request){
        $this->request = $request;
    }

}
