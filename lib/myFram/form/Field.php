<?php
namespace MyFram\Form;
use MyFram\Traits;
use MyFram\Validator;

abstract class Field{
    use Traits\Hydrator;

    protected $errorMessage;
    protected $label;
    protected $name;
    protected $validators = [];
    protected $value;

    public function __construct(array $options = []){
        if(!empty($options)){
            $this->hydrate($options);
        }
    }
    abstract public function buildWidget();

    public function isValid(){
        foreach($this->validators as $validator){
            if(!$validator->isValid($this->value)){
                $this->errorMessage = $validator->errorMessage();
                return false;
            }
        }
        return true;
    }

    public function label(){
        return $this->label;
    }
    public function name(){
        return $this->name;
    }
    public function value(){
        return $this->value;
    }

    public function validator(){
        return $this->validators;
    }

    public function setLabel($label){
        if(is_string($label)){
            $this->label = htmlspecialchars($label,ENT_QUOTES);
        }
    }
    public function setName($name){
        if(is_string($name)){
            $this->name = htmlspecialchars($name,ENT_QUOTES);
        }
    }

    public function setValidators(array $validators){
        foreach($validators as $validator){
            if($validator instanceof Validator && !in_array($validator, $this->validators)){
                $this->validators[] = $validator;
            }
        }
    }

    public function setValue($value){
        if(is_string($value)){
            $this->value = htmlspecialchars($value,ENT_QUOTES);
        }
    }
}
